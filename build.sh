#!/usr/bin/env bash

prog=ucloud_kafka_exporter
version=0.3.0

# 交叉编译
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags " \
-X main.versions=$version \
-X main.branch=`git rev-parse HEAD` \
-X main.buildData=`date -u '+%Y-%m-%d_%H:%M:%S'` \
" -v -o $prog main.go

# 交叉编译 mac test
CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 go build -ldflags " \
-X main.versions=$version \
-X main.branch=`git rev-parse HEAD` \
-X main.buildData=`date -u '+%Y-%m-%d_%H:%M:%S'` \
" -v -o ${prog}_darwin main.go
