# 简介：
该项目是在[danielqsj/kafka_exporter](https://github.com/danielqsj/kafka_exporter) 的基础上做了二次封装。
支持IDC云商Ucloud的Ukafka服务采集监控数据。

```shell script
usage: main [<flags>]

Flags:
  -h, --help                  Show context-sensitive help (also try --help-long and --help-man).
      --web.listen-address=":59308"  
                              Address to listen on for web interface and telemetry.
      --web.telemetry-path="/metrics"  
                              Path under which to expose metrics.
      --ucloud.privatekey=""  Ucloud api PrivateKey.
      --ucloud.publickey=""   Ucloud api PublicKey.
      --version               Show application version.

```