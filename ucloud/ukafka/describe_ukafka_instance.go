package ukafka

import (
	"github.com/ucloud/ucloud-sdk-go/ucloud/request"
	"github.com/ucloud/ucloud-sdk-go/ucloud/response"
)

// DescribeULBRequest is request schema for DescribeULB action
type DescribeUKafkaInstance struct {
	request.CommonBase

	ClusterInstanceId *string `required:"true"`

	Filter *string `required:"false"`
}

type DescribeUKafkaInstanceResponse struct {
	response.CommonBase

	ClusterSet []ClusterSet
}

func (c *UKafkaClient) NewDescribeUKafkaInstance() *DescribeUKafkaInstance {
	req := &DescribeUKafkaInstance{}

	// setup request with client config
	c.Client.SetupRequest(req)

	// setup retryable with default retry policy (retry for non-create action and common error)
	req.SetRetryable(true)
	return req
}

func (c *UKafkaClient) DescribeUKafkaInstance(req *DescribeUKafkaInstance) (*DescribeUKafkaInstanceResponse, error) {
	var err error
	var res DescribeUKafkaInstanceResponse

	err = c.Client.InvokeAction("DescribeUKafkaInstance", req, &res)
	if err != nil {
		return &res, err
	}

	return &res, nil
}
