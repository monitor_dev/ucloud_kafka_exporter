package ukafka

import (
	"github.com/ucloud/ucloud-sdk-go/ucloud/request"
	"github.com/ucloud/ucloud-sdk-go/ucloud/response"
)

// DescribeULBRequest is request schema for DescribeULB action
type ListUKafkaConsumers struct {
	request.CommonBase

	ClusterInstanceId *string `required:"true"`
}

type ListUKafkaConsumersResponse struct {
	response.CommonBase

	TotalCount int

	Groups []ConsumersGroups
}

func (c *UKafkaClient) NewListUKafkaConsumers() *ListUKafkaConsumers {
	req := &ListUKafkaConsumers{}

	// setup request with client config
	c.Client.SetupRequest(req)

	// setup retryable with default retry policy (retry for non-create action and common error)
	req.SetRetryable(true)
	return req
}

func (c *UKafkaClient) ListUKafkaConsumers(req *ListUKafkaConsumers) (*ListUKafkaConsumersResponse, error) {
	var err error
	var res ListUKafkaConsumersResponse

	err = c.Client.InvokeAction("ListUKafkaConsumers", req, &res)
	if err != nil {
		return &res, err
	}

	return &res, nil
}
