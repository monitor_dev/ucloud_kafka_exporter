package ukafka

import (
	"log"

	"github.com/ucloud/ucloud-sdk-go/ucloud/request"
	"github.com/ucloud/ucloud-sdk-go/ucloud/response"
)

// DescribeULBRequest is request schema for DescribeULB action
type UKafkaConsumers struct {
	request.CommonBase

	ClusterInstanceId *string `required:"true"`

	ConsumerGroup *string `required:"true"`

	Type *string `required:"true"`
}

type UKafkaConsumersResponse struct {
	response.CommonBase

	GroupName string
	Type      string

	Topics []*TopicsInfo
}

func (c *UKafkaClient) NewDescribeUKafkaConsumers() *UKafkaConsumers {
	req := &UKafkaConsumers{}

	// setup request with client config
	c.Client.SetupRequest(req)

	// setup retryable with default retry policy (retry for non-create action and common error)
	req.SetRetryable(true)
	return req
}

func (c *UKafkaClient) DescribeUKafkaConsumers(req *UKafkaConsumers) (*UKafkaConsumersResponse, error) {
	var err error
	var res UKafkaConsumersResponse

	err = c.Client.InvokeAction("DescribeUKafkaConsumer", req, &res)
	if err != nil {
		log.Println("res :", &res, &res.Topics)
		return &res, err
	}

	return &res, nil
}
