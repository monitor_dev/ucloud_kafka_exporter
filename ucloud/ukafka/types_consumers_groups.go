package ukafka

type ConsumersGroups struct {
	GroupName string

	Type string

	NumOfTopics int
}

type TopicsInfo struct {
	TotalLag   int
	Partitions map[string]*PartitionsInfo
	//LogPer               float64
	//PartitionAssignedPer int
	LastUpdateTime int
	TopicName      string
}

type PartitionsInfo struct {
	Consumer      string
	CurrentOffset int64

	LogEndOffset int64
	Lag          int
	key          string
}
